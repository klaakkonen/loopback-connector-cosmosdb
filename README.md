loopback-connector-cosmosdb
====

LoopBack No-SQL connector for Microsoft Azure CosmosDB.

How to install?
====

Install the connector package to your project:
```
npm install --save loopback-connector-cosmosdb
```

Set-up a new data source that uses the connector. In `datasources.json`, put the following:
```json
{
  "myDataSourceName": {
    "name": "myDataSourceName",
    "connector": "loopback-connector-cosmosdb",
    "host": "YOUR COSMOSDB HOST, TYPICALLY https://myAccount.documents.azure.com:443/",
    "masterKey": "YOUR MASTER KEY",
    "databaseName": "YOUR DATABASE NAME",
    "collectionName": "YOUR COLLECTION NAME",

    "enableCrossPartitionQueries": false,
    "disableDefaultOrdering": false,
    "chargeWarningThreshold": 0,
    "partitionKey": "Collection partition key property"
  }
}
```

You can name your data source as you like, here we used `myDataSourceName` as an example. You can find values for `YOUR COSMOSDB HOST`, `YOUR MASTER KEY`, `YOUR DATABASE NAME` and `YOUR COLLECTION NAME` in Azure portal. Please consult Azure documentation if needed. Rest of the properties are optional, and default values (shown above) will be used if nothing else is specified.

When configuring model properties, you may want to set specific settings for CosmosDB. For example:
```json
{
  "myViewProperty": {
    "cosmosdb": {
      "propertyName": "myDatabaseProperty"
    }
  }
}
```

Special CosmosDB settings are set via `cosmosdb` configuration object, but this is not required -- connector will use its default values if nothing else is specified in your model configuration. Currently the `partitionKey` property has to be first level object property (without "/"). Please consult LoopBack documentation for more details on how to configure models.

You are now ready to start using CosmosDB in your LoopBack application!

Supported features
====

The following LoopBack's base connector methods are implemented:

* connect()

* disconnect()

* create()

* updateOrCreate()

* replaceOrCreate()

* replaceById()

* all()

* save()

* count()

* update()

* destroyAll()

* updateAttributes()

Connector debug string is `loopback:connector:cosmosdb` that can be enabled through `DEBUG` environment variable.

Change log
===

* 0.0.1 -- Initial release.

* 0.0.2 -- Add support to cross-partition queries. Support `filter.orderBy`.

* 0.1.0 -- Add support to where filtering operators (see https://loopback.io/doc/en/lb3/Where-filter.html#operators). All but `near`, `between` and `regexp` are supported.

* 0.1.2 -- Non-model properties are dropped when saving documents.

* 0.1.3 -- Fix `inq` and `nin` where filtering operators.

* 0.2.0 -- Add possibility to map database properties to different view properties. Fix sub-level logical query construction in where filtering. Support `between` operator in where filtering.

* 0.2.1 -- Fix `lt` and `lte` where filtering operators.

* 0.3.0 -- Add support case-insensitive properties.

* 0.3.1 -- Fix error that resulted when filtering by properties that are not in model.

* 0.4.0 -- Fix `order` where filtering. Support array types. Add `any` where filter that works with array properties. Fix bug https://bitbucket.org/houstoninc/loopback-connector-cosmosdb/issues/1/if-a-model-has-a-property-named-value-a.

* 0.4.1 -- Fix how logical clauses are constructed in where filtering.

* 0.4.2 -- Handle empty logical clauses correctly.

* 0.5.0 -- Add support for request retrying. Add workaround for LOWER() bug in CosmosDB.

* 0.5.1 -- Retry also in `destroyAll` and `update` functions.

* 0.5.2 -- Fix bug in `update` function. Fix document replace and queries to case-insensitive properties.

* 0.5.3 -- Add options `disableDefaultOrdering` and `chargeWarningThreshold`.

* 0.5.4 -- Add low RU `ping` to test connection.

* 1.0.0 -- Update documentdb-dependency to @azure/cosmos. Basic support for cross-partition queries.
